<?php
/**
*   Global Values
*
*  - Grab the page from the uri - it will be used later to include the proper file, as well as other things
*  - The sitename is used for title.php - and as a way to refer to the site easily
*  - domain is used in dbCfg.php to grab the database from mysql, and as an easy way to get the domain 
*  - Debug is set here instead of in the url so malicious EUs can't gain access to debug functions.
**/

$page     = isset($_GET['page']) ? $_GET['page'] : '';
$sitename = "RPM MotorSports of Atlanta";
$domain   = "rpmatl";
$DEBUG    = true;
define('DEV', true);

#admin panel.
$SERVICE = false;
define('MAIN_IMG_WIDTH', 640);
define('MAIN_IMG_HEIGHT', 480);
define('THUMB_IMG_WIDTH',181)

?>