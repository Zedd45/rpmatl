<?php
/**
* This is where we set up what 'stage' the form is in processing.
* We have the following actions so far: view (EU), verify (EU w/ post data), and email (send mail to client)
* the action variable in the url will determine which version of the form shows up
* Recipients sets up where the email gets sent.
*/
$action = isset($_GET['action']) ? $_GET['action'] : 'redirect';

/* 
*  set up redirects for reaching the form by non-standard means - such as hitting the form with a non-standard action, 
*  or trying to hit a validation/email page before you have submitted the first page
*  EXCEPT the inventory pages - which use a form to get data from MYSQL
*/

if ($action!='view' && $action!='verify' && $action!='email') $action='redirect';
if ( ($action=='verify' || $action=='email') && !isset($_POST['submitted']) ) $action='redirect';

if ($action=='redirect') header( 'Location: ?page='.$page.'&action=view' ) ;	

//Set email receipients here 
//$recipients =  "carlo_wilkes@yahoo.com; carlo@rpmatl.com";
$recipients = array("Carlo" => "carlo@rpmatl.com", "Rick" => "rick@rpmatl.com","Ross" => "ross@rpmatl.com", "Tim" => "tim@rpmatl.com", "Paul" => "Paul@rpmatl.com", "Greg" => "Greg@rpmatl.com", "Lorenzo" => "Lorenzo@rpmatl.com");

	
function valueOf($formField){
	isset($_POST[$formField]) ? trim(strip_tags(print($_POST[$formField]))) : print('');
}

function selectedValueOf($formField){
	isset($_POST[$formField]) ? print(' <span class="hilite">'.htmlspecialchars(trim(strip_tags($_POST[$formField]))).'</span> ') : print('');
}


if ($action=="view"){ 
?>

<form name="<?php echo $page; ?>" method="post" action="<?php echo "?page=$page&amp;action=verify"; ?>">

<?php 
}//end view 

if ($action=="verify"){
?>	

<p>Please confirm that the information you submitted is correct.  Click &quot;edit&quot; to make changes.</p>

<form name="<?php echo $page; ?>" method="post" action="<?php echo "?page=$page&amp;action=email"; ?>">

<?php 
}//end verify 

if($action=='email'){
	print('<p>Your Form Was Submitted Successfully. Thank you!</p>');
}//end email

?>