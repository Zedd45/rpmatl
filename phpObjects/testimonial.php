<?php
class Testimonial {
	
	private $result;
	private $count;
	private $offset;
	private $limit;
	
	function __construct($offset=0, $limit=5) {
		$countQuery = "SELECT COUNT(*) FROM testimonials;";
		$countResult = mysql_query($countQuery);
		$row = mysql_fetch_row($countResult);
		$this->count = $row[0];
		$this->offset = $offset;
		$this->limit = $limit;
		$query = "SELECT * FROM testimonials ORDER BY testimonial_id DESC LIMIT " . mysql_real_escape_string($offset) . ", " . mysql_real_escape_string($limit) . ";";
		$this->result = mysql_query($query);
	}
	
	function __destruct() {
		unset($query);
		unset($result);
		unset($row);
		unset($this->count);
		unset($this->offset);
		unset($this->limit);
		unset($this->result);
	}
	
	public function count() {
		return $this->count;	
	}
	
	public function next() {
		$row = mysql_fetch_array($this->result);
		if (!$row) return false;
		return $row;
	}
	
	public function range() {
		$r['low'] = $this->offset + 1;
		$r['high'] = $this->offset + $this->limit;
		if ($r['high'] > $this->count)
			$r['high'] = $this->count;
		return $r;
	}
	
	public function link_line($href) {
		$a = "";
		for ($i = 0; $i < $this->count; $i=$i + $this->limit) {
			$lower = $i + 1;
			$upper = $i + $this->limit;
			if ($upper > $this->count)
				$upper = $this->count;
			if ($i == $this->offset)
				$a = $a . "[" . $lower . " - " . $upper . "] ";
			else
				$a = $a . "[<a href=\"".$href."&offset=" . $i . "&limit=" . ($this->limit) . "\">" . $lower . " - " . $upper . "</a>] ";
		}
		return $a;
	}
}
?>