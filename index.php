<?php 
# Start output buffering so we can send headers depending on the page (so we can send redirects).
ob_start();

require_once "phpcfg/config.php";
require_once "phpcfg/dbCfg.php";
require_once 'phpinc/globalUtils.php';
require_once "phpinc/header.php";
require_once "phpinc/template.php";

/** 
*   if the page is set, use it - otherwise, load the homepage.
*   if the page does not exist, warn the user with a custom 404 page.
**/
if ($page!='') {
	$page = str_replace("..", "", $page);
	if (file_exists('pages/' . $page . '.php')){
		include 'pages/' . $page . '.php';
	}
	else{
		?>
		<h3>We could not find the page you specified!</h3>
		<p>Please try <a href="javascript:history.go(-1);">going back</a>.</p>
		<p>You can also start over at our <a href="?page=home">homepage</a>.</p>
		<?php
	}
}
else {
	//redirect to the homepage, so you get the correct variable call
	header('Location: ?page=home');
}

ob_end_flush();
require_once "phpinc/footer.php";

//destroy variables in memory
unset($page);
unset($sitename);
unset($domain);
unset($DEBUG);

// Free resultset
mysql_free_result($carResult);
// Close connection
mysql_close($link);


/*
# LEGACY CODE!  
# This was used to prevent casual viewers from accessing the site during construction.


$loggedIn = false;

//check that we received a user name & password
if ( isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']) ) {

	$userQuery = "SELECT user_id FROM admin WHERE user_name = '{$_SERVER['PHP_AUTH_USER']}' and password=PASSWORD('{$_SERVER['PHP_AUTH_PW']}') LIMIT 1";
	$userResult = mysql_query($userQuery);
	$row = mysql_fetch_array($userResult);

	//if any rows are returned, log in!
	if($row){
		$loggedIn=true;
	}
}

if (!$loggedIn){
	header ('WWW-Authenticate: Basic Realm="RPM Admin"');
	header('HTTP/1.0 401 Unauthorized');  //if EU hits cancel, give him / her a 401 error
	exit();
}
*/
?>