<?php
require_once "../phpcfg/config.php"; 
require_once "../phpcfg/dbCfg.php";
#not sure why this one needed doc root, but even with ../ (the pattern for the two preceeding files) it would fail to find the file.
require_once  $_SERVER['DOCUMENT_ROOT'] . '/phpObjects/SimpleImage.php';


# CR#1 - SOLD
function printSoldImg(){
    echo '<img src="../external/images/sold_indicator.png" class="sold" alt="SOLD!">';
}

/**
 * returns a mysql result set for $count cars
 * @param $count - integer limit for cars to return.  false will return no limit.
 * @return mysql result
 */
function getCarDataForThumbs($count = 5){
  connectRPM();  
  $carSelector = mysql_real_escape_string("cars.imgName, cars.caryear, make.make, model.model, cars.price, cars.milage, cars.car_id, cars.sold");
  # make a connection to the database and print the newest cars by forming a query from the most recently updated entries to the DB (get the date_submitted)  
  $carOrder    = mysql_real_escape_string("cars.date_submitted DESC");
  $carWhere    = mysql_real_escape_string("cars.make_id = make.make_id AND cars.model_id = model.model_id");
  $carQuery    = "SELECT $carSelector FROM cars, make, model WHERE $carWhere ORDER BY $carOrder LIMIT $count;";
  printQuery($carQuery);
  $carResult   = mysql_query($carQuery) or die(mysql_error());
  return $carResult;
}

function getThumbnailForCar(){
  
}

/**
 * @param $car_id = the ID of the car in the DB
 * @param carString - the data to print for title & alt - preferably the car information
 * @return string image for car
 */
function getCarImageById($car_id,$carString = 'vehicle'){
  $imgQuery  = "SELECT image_location FROM car_img WHERE car_id ='$car_id' ORDER BY img_id LIMIT 1;";
	$imgResult = mysql_query($imgQuery);
	$imgPath = mysql_fetch_array($imgResult);
	$image = $imgPath['image_location'];
	$imageLocation = file_exists($_SERVER['DOCUMENT_ROOT'] . IMGPATH . $image) ? IMGPATH . $image : TESTIMGPATH . $image;
	return   "<img src=\"$imageLocation\" alt=\"$carString image\" title=\"Show more details for $carString\" />";
	
}

/**
 * checks if there is an entry for the thumbnail in the DB (legacy) or creates a thumb on the fly
 * @param $car_id = the ID of the car in the DB
 * @param $imageFileName = String for filename location.  
 * @param carString - the data to print for title & alt - preferably the car information
 * @return string image for car
 */
function getCarThumbImage($car_id, $imageFileName, $carString = 'vehicle') {
  $imageLocation = getFullImagePathByFileName($imageFileName);
  if (false !==  $imageLocation){
      return   "<img src=\"$imageLocation\" alt=\"$carString image\" title=\"Show more details for $carString\" />";
  }
  else {
    return getThumbNailByCarId($car_id, $carString);
  }	
}

/**
 * fallback if there is no image
 * @param $car_id = the ID of the car in the DB
 * @param carString - the data to print for title & alt - preferably the car information
 * @return string image for car
 */
function getThumbNailByCarId($car_id, $carString = 'vehicle'){
  $SI_handler = new SimpleImage();
  $comingSoonThumb = "<img src=\"" . IMGPATH . "coming_soon_thumb.png\" alt=\"$carString image\" title=\"Show more details for $carString\" />";
  
  if (false){
      
  }
  else {
    return  $comingSoonThumb;
  }
}

function printQuery($query){
  if ($DEBUG){
    $string = "qruery preformed: $query";
    echo "<script>console.log('$string')</script>";
    error_log($string, 2, WEBMASTER);
  }
}

/**
 * function designed to calculate paths for testing image manipulation tools.
 * @param $fileName string - file name of image with no path (ie. picture.png)
 * @return $filePath string - the fully qualified path to the image. False if no image can be found on the server
 * NOTE: returns false if no image can be found on the server
 */
function getFullImagePathByFileName($fileName){
  $imagePath = $_SERVER['DOCUMENT_ROOT'];
  $standardPath = IMGPATH . $fileName;
  $testPath = TESTIMGPATH . $fileName;
  
  echo "stdpath: $standardPath.  testpath: $testPath";
  
  if (file_exists($imgPath . $standardPath)){ 
    return $standardPath; 
  }
  else if (file_exists($imgPath . $testPath)){ 
    return $testPath; 
  }
  else { 
    return false; 
  }
}

?>