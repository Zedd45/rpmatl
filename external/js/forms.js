/**
* validate takes an array of fieldNames and processes logic on them to check if they are blank - using regX on the fieldNames to determine if they need addt'l validation (such as email).
* @Param fieldNames - a Jquery object containing all of the inputs that require validation
* at a later date I plan to add regX to check for things like invalid email addresses or text in the phone number fields
**/
function validate(fieldNames){
	var passed=true;
	var errFields = "";
	
	$(fieldNames).each(function(){
		if ($(this).value==''){
			passed = false;
			//the previous sibling should be the <td> with the field label.
			errFields += $(this).prev().html();
		}
	})
	
	if(!passed) displayErr(errFields);
	return passed;
}

function isBlank(fieldName){
	var friendlyFieldLabel = $("input[name='"+fieldName+"']").prev().html();
	console.log(friendlyFieldLabel);
	
	if ( $("input[name='"+fieldName+"']").value=='' ){
		passed = false;			
		errLog += friendlyFieldLabel + "\n"; 
	}
}

function displayErr(errLog){
	var err = $('div.errMsg');
	
	$(err).html(errLog);
	$(err).fadeIn('fast');
}

/** 
* bind the onsubmit function to the form 
* Jquery will grab all of the fields inside of the form that have the class 'required' and will send that object to validate
**/
$(function(){
	$('.contentHomeBody form').onsubit(function(){
		return validate($(this).filter('input.required'));
	});
});