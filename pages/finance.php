<?php 
require_once "phpcfg/formConfig.php";
?>

	<h1>Finance a Vehicle With Us</h1>
	
	<h2>Loan Information</h2>
	
	<table id="loanInfo">
		<tr>
			<td>
				Amount Required
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="amount" size="20" class="formField" value="<?php valueOf('amount'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					selectedValueOf('amount');
				} 
				?>
			</td>
			<td class="top">
				Loan Term:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<select name="term">
				<?php
				$termOptions = array('36','48','60','66','72','84','96');
				foreach ($termOptions as $value){
					print("<option value=\"$value Months\">$value Months</option>\n");
				}
				?>
				</select>
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					echo selectedValueOf('term');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				Down Payment:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="downPayment" size="20" class="formField" value="<?php valueOf('downPayment'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					selectedValueOf('downPayment');
				} 
				?>
			</td>
			<td>
				Trade-In:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<select name="tradeIn">
					<option value="yes">Yes</option>
					<option value="no">No</option>
				</select>
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					selectedValueOf('tradeIn');
				} 
				?>
			</td>
		</tr>
	</table>
	
	<hr/>
	
	<h2>Desired Vehicle</h2>
	
	<table id="desiredVehicle">
		<tr>
			<td>
				Manufacturer:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="manufacturer" size="20" class="formField" value="<?php valueOf('manufacturer'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					selectedValueOf('manufacturer');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				Year:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="year" size="20" class="formField" value="<?php valueOf('year'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					selectedValueOf('year');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				Model:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="model" size="20" class="formField" value="<?php valueOf('model'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					selectedValueOf('model');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				Miles:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="miles" size="20" class="formField" value="<?php valueOf('miles'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					selectedValueOf('miles');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				Vin Number:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="vin" size="20" class="formField" value="<?php valueOf('vin'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					selectedValueOf('vin');
				} 
				?>
			</td>
		</tr>
	</table><!--desiredVehicle-->
		
	<hr/>
	
	<h2>Employment Information</h2>
	
	<table id="empInfo">
		<tr>
			<td>
				*Employer:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="employer" size="20" class="formField" value="<?php valueOf('employer'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					selectedValueOf('employer');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				*Occupation:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="occupation" size="20" class="formField" value="<?php valueOf('occupation'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					selectedValueOf('occupation');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				*Monthly Income:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="monthlyIncome" size="20" class="formField" value="<?php valueOf('monthlyIncome'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					selectedValueOf('monthlyIncome');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				Time on Job:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<select name="yearsOnJob">
					<option value="">Years</option>
					<option value="1 Year">1 Year</option>
				<?php
				for ($i=2; $i<10; $i++){
					echo("<option value=\"$i Years\">$i Years</option>");
				}
				?>
					<option value="10+ Years">10+ Years</option>
				</select>
				&nbsp;
				<select name="monthsOnJob">
					<option value="">Months</option>
					<option value="1 Month">1 Month</option>
				<?php
				for ($i=2; $i<=11; $i++){
					echo("<option value=\"$i Months\">$i Months</option>");
				}
				?>
				</select>
				<?php
				}//end view
				if ($action=='verify' || $action=='email'){
					selectedValueOf('yearsOnJob') .'&nbsp;'. selectedValueOf('monthsOnJob');
				}
				?>
			</td>
		</tr>
		<tr>
			<td>
				*Business Phone:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="busPhone" size="20" class="formField" value="<?php valueOf('busPhone'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					selectedValueOf('busPhone');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				*Address:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="busAddress" size="20" class="formField" value="<?php valueOf('busAddress'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					selectedValueOf('busAddress');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				City
			</td>
			<td>
				<?php if($action=='view'){ ?>
				<input type="text" name="busCity" size="20" class="formField" value="<?php valueOf('busCity'); ?>"/>
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('busCity');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				State
			</td>
			<td>
				<?php if($action=='view'){ ?>
				<input type="text" name="busState" size="20" class="formField" value="<?php valueOf('busState'); ?>"/>
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('busState');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				Zip
			</td>
			<td>
				<?php if($action=='view'){ ?>
				<input type="text" name="busZip" size="20" class="formField" value="<?php valueOf('busZip'); ?>"/>
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('busZip');
				} 
				?>
			</td>
		</tr>
	</table><!--empInfo-->
	
	<hr/>
	
	<h2>Other Income</h2>
	
	<table id="otherIncome">
		<tr>
			<td>
				Source
			</td>
			<td>
				<?php if($action=='view'){ ?>
				<input type="text" name="source" size="20" class="formField" value="<?php valueOf('source'); ?>"/>
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('source');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				Monthly Income
			</td>
			<td>
				<?php if($action=='view'){ ?>
				<input type="text" name="otherMontlyIncome" size="20" class="formField" value="<?php valueOf('otherMontlyIncome'); ?>"/>
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('otherMontlyIncome');
				} 
				?>
			</td>
		</tr>
	</table>
	
	<hr/>
	
	<h2>Contact Information</h2>
	
    <table id="contactInfo">
		<?php include 'phpinc/contactInfo.php'; ?>
	</table><!--contactInfo -->
	
    <hr/>

	<h2>Applicant Information</h2>
	
	<table id="applicantInfo">
		<tr>
			<td>
				Date of Birth:
			</td>
			<td>
				<?php if($action=='view'){ ?>
				<input type="text" name="dobMonth" size="2" maxlength="2" class="formField" value="<?php valueOf('dobMonth'); ?>"/>
				<input type="text" name="dobDay" size="2" maxlength="2" class="formField" value="<?php valueOf('dobDay'); ?>"/>
				<input type="text" name="dobYear" size="2" maxlength="2" class="formField" value="<?php valueOf('dobYear'); ?>"/>
				<em>(MM/DD/YY)</em>
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 print(selectedValueOf('dobMonth') .'/'.selectedValueOf('dobDay').'/'.selectedValueOf('dobYear'));
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				*Residence Type
			</td>
			<td style="right">
				<?php if($action=='view'){ ?>
				<select name="resType">
					<option value="Own">Own</option>
					<option value="Rent">Rent</option>
				</select>
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('resType');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				*Monthly Payment:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="monthlyPayment" size="20" class="formField" value="<?php valueOf('monthlyPayment'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					selectedValueOf('monthlyPayment');
				} 
				?>
			</td>
		</tr>	
		<tr>
			<td>
				*Time at Residence:
			</td>
			<td>
				<?php if($action=='view'){ ?>
				<select name="timeAtResidence">
					<option value="">Years</option>
					<option value="1 Year">1 Year</option>
				<?php
				for ($i=2; $i<10; $i++){
					echo("<option value=\"$i Years\">$i Years</option>");
				}
				?>
					<option value="10+ Years">10+ Years</option>
				</select>
				&nbsp;
				<select name="monthsAtResidence">
					<option value="">Months</option>
					<option value="1 Month">1 Month</option>
				<?php
				for ($i=2; $i<=11; $i++){
					echo("<option value=\"$i Months\">$i Months</option>");
				}
				?>
				</select>
				<?php
				}//end view
				if($action=='verify' || $action=='email'){
					selectedValueOf('timeAtResidence') .'&nbsp;' . selectedValueOf('monthsAtResidence');
				}
				?>
			</td>
		</tr>
	</table>
	
	<hr/>
 
	<h2>Additional Information</h2>
	
	<table>
		<tr>
			<td class="top">
				Message Text:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<textarea name="message" cols="30" rows="5" class="formField"><?php valueOf('message'); ?></textarea>
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('message');
				} 
				?>
			</td>
		</tr>
<?php
require_once 'phpinc/submitButtons.php';
//destroy variables in memory
unset($action);
?>
</table>