<?php
require_once "phpcfg/formConfig.php";
?>
	<h1>Sell Us Your Vehicle</h1>

	<table id="sell">
		<tr>
			<td>
				Make:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="make" size="20" class="formField" value="<?php valueOf('make'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('make');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				Model:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="model" size="20" class="formField" value="<?php valueOf('model'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('model');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				*Color, Options, ect.
			</td>
			<td>
				<?php if($action=='view'){ ?>
				<textarea name="options" cols="30" rows="5" class="formField"><?php valueOf('options'); ?></textarea>
				<?php
				}//end view
				if($action=='verify' || $action=='email'){
					selectedValueOf('options');
				}
				?>
				
			</td>
		</tr>
<? 
include 'phpinc/contactInfo.php';
require_once 'phpinc/submitButtons.php';
?>
</table>