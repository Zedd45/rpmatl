<?php
/**
*  Set up the connection to the DB and grab the car ID from the url (get variable).
*  If no ID exists, warn the user, and halt the script.
*  Otherwise, print the data in a table.
**/
require_once "phpcfg/dbCfg.php";

$carId = isset($_GET['carid']) ? $_GET['carid'] : '';

if ($carId == '') :
    include_once 'phpinc/fragments/showCaseNoResults.php';
    if ($DEBUG) : echo '<span style="line-height:0;">car id not specified</span>'; endif;
else :


//start the query
$selector  = "cars.*, make.make, model.model";
$tables    = "cars, make, model";
$where     = "cars.car_id = $carId AND cars.make_id = make.make_id AND cars.model_id = model.model_id";

$carQuery  = "SELECT $selector FROM $tables WHERE $where LIMIT 1;";
$carResult = mysql_query($carQuery);

/* DEBUG STARTS HERE */
if ($DEBUG){ ?>
	<script type="text/javascript">
		console.log('showCase Query: <?php echo $carQuery ?>');		
	</script>
<?php 
}//end debug

//use assoc here so we can parse it out as we want.  There should only be one result, after all...
$data = mysql_fetch_array($carResult,MYSQL_ASSOC);

//build the string for titles and alt attributes
$carString = $data['caryear'].' '.$data['make'].' '.$data['model'];

$imgsQuery  = "SELECT image_location FROM car_img WHERE car_id = $carId ORDER BY img_id;";
$imgsResult = mysql_query($imgsQuery);


if (null == $data['car_id']){ 
    include_once 'phpinc/fragments/showCaseNoResults.php';
    if ($DEBUG) { echo '<span style="line-height:0;">car id not found in DB</span>'; }
}
else {
    include_once 'phpinc/fragments/showCaseDisplay.php';
}
#end SQL processing
endif;
?>