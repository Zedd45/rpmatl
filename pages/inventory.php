<?php
require_once "phpcfg/dbCfg.php";

/* functions for restoring or validating user input */

function valueOf($formField){
	isset($_GET[$formField]) ? print(strip_tags(trim($_GET[$formField]))) : print('');
}


/* define the default options */ 

//$limit       = 15;
$carSelector = "cars.imgName, cars.caryear, make.make, model.model, cars.price, cars.milage, cars.car_id, cars.sold";
$carWhere    = "cars.make_id = make.make_id AND cars.model_id = model.model_id";
$carOrder    = "make.make";

$carQuery = "SELECT $carSelector FROM cars, make, model WHERE $carWhere ORDER BY $carOrder"; //LIMIT $limit; - don't limit as there is no way to search at this time
$carResult = mysql_query($carQuery);

/* DEBUG STARTS HERE */
if ($DEBUG){ ?>
	<script type="text/javascript">
		console.log('inventory Query: <?php echo $carQuery ?>');
	</script>
<?php 
}//end debug
?>

<h1>View our Inventory Below</h1>

<?php 
//if no results returned...
if(mysql_num_rows($carResult)<1) print '<p class="msg">We were unable to locate any vehicles with the specified criteria.  Please try your search again.</p>';
?>

<div class="listCars">
	<?php
	 	//reset the variable that decides the <h3>
		$newest=false;
		include "phpinc/printCars.php"; 
	?>
</div><!-- listCars -->

<?php


/* LEGACY CODE!


    /* not used - 10-15 cars at most per Carlo 
    //update with options from the user by checking that submit (hidden input) is present in the GET array 
    if (isset($_GET['submitted'])) {

    	// use JS to show the form again - regardless of an error or correct input ? might change to error later
    	print('<script type="text/javascript">$(function(){toggleOptions();});</script>');

    	//assign the get variables to a programmer-friendly name - no need to strip or trim; values are populated from the DB.
    	$limit = $_GET['npp'];
    	$make  = $_GET['make'];
    	$model = $_GET['model'];
    	$year  = $_GET['year'];
    	$color = $_GET['color'];
    	//$price = $_GET['price']; //later

    	$carSelector = "cars.imgName, cars.year, make.make, model.model, cars.price, cars.milage, cars.car_id";
    	$carWhere    = "cars.make_id = make.make_id AND cars.model_id = model.model_id";
    	$carOrder    = "make.make";

    	//check the data before building a call to the DB.  if it's empty, print a message and exit the script.
    	if(!$model && !$make && !$year && !$color) { //&& !$price) {
    		print('<p class="msg">You did not submit any information.  Please try again.</p>');
    		exit();
    	}
    	*/

    	/**
    	*  Detect which variables have been submitted by the user, and adjust the query accordingly 
    	*  If the field isn't empty - add the query.
    	**/

    	/* see above - not used on this site
    	//make set
    	if($make!='') {
    		$carWhere += "make.make LIKE '$make' AND";
    	}

    	//only model set
    	if($model!='') {
    		$carWhere  = "model.model LIKE '$model%' AND cars.make_id = make.make_id AND cars.model_id = model.model_id";
    	}

    	//only color set
    	else if(!$model && !$make && !$year) {
    		$carWhere  = "cars.color LIKE '$color' AND cars.make_id = make.make_id AND cars.model_id = model.model_id";
    	}

    	//only year set
    	else if(!$model && !$make && !$color) {
    		$carWhere  = "cars.year LIKE '%$year%' AND cars.make_id = make.make_id AND cars.model_id = model.model_id";
    	}
    } 
    
    //$makeQuery = "SELECT make, make_id FROM make ORDER BY make.make;";
    //$makeResult = mysql_query($makeQuery);

    #later
    <!-- force the options div to show for EUs w/o JS -->
    <!--<noscript> <style type="text/css"> #options { display:block; } </style> </noscript>-->

    <a id="opt"></a>
    <div class="optContainer">

    	<a href="javascript://" onclick="toggleOptions();" class="showMore">Show More Options</a>

    	<div id="options">
    		<form name="options" method="get" action="<?php echo "?page=$page"; ?>">

    			<!-- this hidden input helps the server keep track of where we are; the GET will override the page var -->
    			<input type="hidden" name="page" value="<?php echo $page; ?>"/>
    			<input type="hidden" name="submitted" value="1"/>

    			<table>
    				<tr>
    					<td>
    						Results per page:
    					</td>
    					<td>
    						<select name="npp">
    							<?php
    							$limitValues = array(10,15,20,25,30);
    							foreach($limitValues as $number){
    								print('<option value="'.$number.'"');
    								if ($limit==$number) print (' selected="selected"');
    								print(">$number</option>");
    							}
    							?>
    						</select>
    					</td>
    				</tr>
    				<tr>
    					<td>
    						Make:
    					</td>
    					<td>
    						<select name="make">
    							<option value="">Select One</option>
    							<? while ($row = mysql_fetch_array($makeResult))  {
    								?><option value="<? echo $row['make']; ?>"><? echo $row['make']; ?></option>><?
    								}
    							?>
    						</select>
    					</td>
    				</tr>
    				<tr>
    					<td>
    						Model:
    					</td>
    					<td>
    						<input type="text" name="model" value="<? valueOf('model') ?>" class="formField"/>
    					</td>
    				</tr>
    				<tr>
    					<td>
    						Color:
    					</td>
    					<td>
    						<input type="text" name="color" value="<? valueOf('color') ?>" class="formField"/>
    					</td>
    				</tr>
    				<tr>
    					<td>
    						Year:
    					</td>
    					<td>
    						<input type="text" name="year" value="<? valueOf('year') ?>" class="formField"/>
    					</td>
    				</tr>
    				<!--<tr>
    					<td>
    						Price:
    					</td>
    					<td>
    						<input type="text" name="price" value="<? valueOf('price') ?>" class="formField"/>
    					</td>
    				</tr> -->

    				<tr>
    					<td colspan="2" style="text-align:center;">
    						<input type="image" name="submit" src="external/images/btn_submit.jpg" alt="Submit"/>
    						<a href="javascript:document.forms['options'].reset();"><img src="external/images/btn_reset.jpg" alt="Reset"/></a>
    					</td>
    				</tr>
    			</table>
    		</form>
    	</div><!-- options -->

    </div><!-- optContainer -->
    

<!-- show options at top and bottom so EUs don't have to scroll -->
<!--<p><a href="#opt" onclick="toggleOptions();" class="showMore">Show More Options</a></p>-->

*/
?>