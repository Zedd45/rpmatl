<?php
require_once "phpcfg/formConfig.php";
?>

	<h1>Allow Us to Locate a Vehicle for You!</h1>
		
	<table id="locate">
		<tr>
			<td>
				Make:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="make" size="20" class="formField" value="<?php valueOf('make'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('make');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				Model:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="model" size="20" class="formField" value="<?php valueOf('model'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('model');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				Maximum Price:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="maxPrice" size="20" class="formField" value="<?php valueOf('maxPrice'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('maxPrice');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				Minimum Price:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="minPrice" size="20" class="formField" value="<?php valueOf('minPrice'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('minPrice');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				Maximum Milage:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="maxMilage" size="20" class="formField" value="<?php valueOf('maxMilage'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('maxMilage');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				Minimum Milage:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="minMilage" size="20" class="formField" value="<?php valueOf('minMilage'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('minMilage');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td>
				*Color, Options, ect.
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<textarea name="options" cols="30" rows="5" class="formField"><?php valueOf('options'); ?></textarea>
				<?php
				}//end view
				if($action=='verify' || $action=='email'){
					selectedValueOf('makeModel');
				}//end verify
				?>
			</td>
		</tr>
<?php 
include      'phpinc/contactInfo.php';
require_once 'phpinc/submitButtons.php';
?>
</table>