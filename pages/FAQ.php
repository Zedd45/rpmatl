<h1>Frequently Asked Questions</h1>

<p class="question">How does RPM Motorsports of Atlanta locate a vehicle that I want to purchase?</p>
<p class="answer">RPM Motorsports of Atlanta has access to the highest quality of pre-owned vehicles available at dealer sales located across the nation.  In addition, our connection with dealerships and leasing companies gives us a keen advantage in locating your vehicle.</p>

<p class="question">How long does it take to locate and deliver my vehicle?</p>
<p class="answer">For most orders RPM Motorsports of Atlanta can locate, purchase, and deliver your vehicle in as little as 2 days up to 21 days after placing your order.  In the case of more specific orders i.e. exotic vehicles, the delivery period may be longer.</p>

<p class="question">Do I need to put down a deposit and is it refundable?</p>
<p class="answer">After you've specified the vehicle that you want, you will need to provide your sales associate a deposit to initiate the search.  This is not a fee; the amount will be credited to your purchase.  In the event we can not satisfy your requirements or meet your expectations, your deposit is refundable.</p>

<p class="question">Are extended warranties available on vehicles purchased?</p>
<p class="answer">If you decide to purchase a vehicle whose original factory warranty is expired, we do offer affordable extended warranties varying in coverage and terms.</p>

<p class="question">How do I get my vehicle once it arrives?</p>
<p class="answer">Once your vehicle has been inspected, serviced and detailed we will provide transportation to our showroom for local clients, provide transportation from the airport to our showroom, schedule a time convenient to your schedule for pickup or we will ship to your doorstep using one of our service providers.</p>

<p class="question">Does RPM Motorsports of Atlanta provide any aftermarket services?</p>
<p class="answer">Using our experienced contractors, we provide an array of professionally installed, high-quality products ranging from wheels, tires, audio/visual equipment, navigation, back-up camera, Bluetooth, window tinting etc. These services are provided at cost to our clients.</p>

<p class="question">Are there any special services provided for special occasions?</p>
<p class="answer">Certainly we do!  We provide special treatments for birthday surprises, graduations, anniversaries, Valentines Day and other occasions that are special to you.  Whatever your heart desires we will do our best to provide!</p>

<p class="question">Does RPM Motorsports of Atlanta provide financing or leasing?</p>
<p class="answer">Yes we do assist our clients in locating the best financing or leasing options for their specific needs.  We have a network of banks and credit unions at our disposal.  We will need a completed application to assist in the process.</p>
