<?php

include_once "phpObjects/testimonial.php";


if (isset($_GET['offset']) && isset($_GET['limit']))
	$testimonials = new Testimonial($_GET['offset'], $_GET['limit']);
else
	$testimonials = new Testimonial();
$range = $testimonials->range();
?>

<h3>Displaying Testimonials <? echo $range['low']; ?> - <? echo $range['high']; ?></h3>
<p><? echo $testimonials->link_line("?page=testimonials"); ?></p>
<?
while ($info = $testimonials->next() ) {
	?>
	<p><img src="external/images/<? echo $info['image_location']; ?>" /><br/>
	<? echo $info['data']; ?>
	</p>
	<?
}
?>

<p><? echo $testimonials->link_line("?page=testimonials"); ?></p>

<?php
//destroy the vars in memory by calling the destrector
$testimonials->__destruct();
?>
