<?php 
require_once "phpcfg/formConfig.php";
?>

	<h1>Contact Us</h1>

	<table id="contactForm">
		<tr>
			<td>
				*Subject
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<input type="text" name="subject" size="20" class="formField required" value="<?php valueOf('subject'); ?>">
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('subject');
				} 
				?>
			</td>
		</tr>
		<tr>
			<td class="top">
				*Message:
			</td>
			<td>
				<?php if ($action=='view'){ ?>
				<textarea name="message" cols="30" rows="5" class="formField required"><?php valueOf('message')?></textarea>
				<?php 
				}
				if ($action=='verify' || $action=='email'){
					 selectedValueOf('message');
				} 
				?>
			</td>
		</tr>
<?php 
include 'phpinc/contactInfo.php'; 
require_once 'phpinc/submitButtons.php';
?>
</table>