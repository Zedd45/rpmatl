
<div id="tabs">
    <ul>
        <li>About Us</li>
        <li>Frequently Asked Questions</li>
    </ul>
</div>
<div id="tabContent">
    <div>
        <p>At <strong><em>RPM Motorsports of Atlanta</em></strong> we have established ourselves as one of the preeminent dealerships to purchase your next new or pre-owned vehicle.  We pride ourselves on our unique ability to find <strong><em>THE</em></strong> perfect vehicle for busy professionals like you whose time is better served on what is important to <strong><em>YOUR</em></strong> schedule. With over 30 years of in-house experience, our &quot;boutique&quot; style of placing vehicles is both fun and efficient. </p>

        <p>Our goal is to provide four essential criteria that are important to a successful process:</p>

        <h2>Effective Use of Your Time</h2>

        <p>Your time is better served while our staff searches for the vehicle you request.  We will consult with our clients to help choose the right vehicle for their lifestyle and budget and based on our vast network, locate your vehicle with no interruption of your daily schedule. Included will be the color, options and model that YOU choose.  Our goal is not to fit you into our inventory but purchase exactly what you want. One stop or one call and we do the rest! </p>

        <h2>Quality of Vehicle</h2>

        <p>Using our locator services, we search inventory across the nation to locate the best vehicle we can find for our clients.  Our vehicles are inspected twice, serviced at one of our participating factory service departments, professionally detailed, and kept inside our 13,000 square foot climate controlled showroom until delivery!  All are <strong><em>CARFAX&reg;</em></strong> checked and carry factory warranties.</p>

        <h2>Fantastic Pricing</h2>

        <p>Our pricing is based on purchasing from <strong><em>WHOLESALE</em></strong> inventories at <strong><em>WHOLESALE</em></strong> prices.  We use our purchasing power to save you real dollars, often thousands less than other retail outlets.  Our business is built on word-of-mouth referrals and we have clients in over 27 states!  Our ability to place a quality vehicle in driveways and garages across the country at affordable prices is what drives our business!</p>

        <h2>Customer Service</h2>

        <p>Remember when business could be confirmed on a handshake or the customer was always right?  Come experience the finest in professionalism, service and assistance the <strong><em>RPM Motorsports of Atlanta</em></strong> way!  Our friendly staff will answer any questions, address any concerns, and give you all of the information needed to make the most informed decision possible. Purchasing a vehicle can be fun and special when done the right way with <strong><em>YOU</em></strong> in mind!</p>

        <p>So remember... Consult with our staff, choose your vehicle, relax and wait for delivery! </p>

        <p>QUESTIONS? Call or <a href="?page=contact" title="send an email with our online form">Email us</a> today at (770) 216-2999 or just fax your order to (770) 216-2998.  We are located at <a href="?page=directions" title="Get Directions to our location">3097-B Presidential Drive, Atlanta, Georgia 30340</a></p>
    </div>
    <div>
        <?php include 'pages/FAQ.php'; ?>
    </div>
</div>
<script type="text/javascript">
    var tabId = '#tabs';
    var tabContent = '#tabContent div';
    
    $(function(){
        //bind event handlers
        $(tabId).live('mouseover mouseout',function(e){
            $(e.target).toggleClass('active');
        });
        $(tabId+' li').live('click',function(){
            var contentIndex = $(tabId + ' li').index(this);
            $(tabContent).each(function(){
                $(this).fadeOut("medium",function(){
                    $(tabContent).eq(contentIndex).fadeIn("medium");
                });
            });
        });
    });
</script>

<?php 
/*  LEGACY CODE
<h1>The RPM Advantage</h1>

<p>Did you know that most people will spend 18% to 20% of their lives in their vehicle?  With that knowledge comes great responsibility according to RPM Motorsports of Atlanta.  After a new home, a vehicle in most cases will be the largest purchase one can make and should never be taken lightly.  Which is why we advise all in the pursuit of finding the car of their choice to be patient and to be informed in order that they may buy smart.  We realize the importance of vehicle selection and have a charge to be one of the premiere dealerships of choice nationwide for potential customers in the market to purchase a vehicle.</p>

<p>We provide the vehicle that you request alongside personalized customer service tailored to your individual lifestyle.  At first glance of one of our business cards it can be assumed that we only provide high-end vehicles. However, we offer a great selection of new and pre-owned vehicles.  So, wether you are looking for the new Mercedes Benz, a Ford SUV or a Honda Civic, RPM Motorsports of Atlanta is the premier dealership for all your vehicle needs.</p>

<p>We stand above and apart from the competition combining the best purchase price along with the best quality for all of our valued customers.  We also offer a variety of financing options to accommodate almost any budget.  Here at RPM Motorsports of Atlanta we thrive off of customer referrals and customer satisfaction.  So come in and experience the RPM Advantage - </p>
	
<p style="text-align:center; font-weight:bold; font-style:italic;">&#34;We will not be undersold, nor will you be under-served!&#34;</p>
	
<h1>The RPM Showroom</h1>

<p>Our 7000 sq. ft. showroom is divinely decorated with the latest in contemporary furniture and boasts of the most advanced in technology to help engage and educate every customer that enters. Oh, and did we mention our showroom floor is lined with the best that 4 wheels have to offer?</p>

<p>
	When a client enters our Atlanta location, they immediately recognize that we mean car business.  Our friendly staff and knowledgeable sales associates aim at leaving a lasting impression that will serve only to compliment your vehicle purchasing experience.  We stand firm knowing that your expectations will be exceeded and your desires will be delivered. 
	<span style="margin-left:25px; font-weight:bold;">RPM + You = No Regrets.</span>
</p> 

<p>Remember, we are conveniently located just off I-85. RPM Motorsports of Atlanta is proud to serve you nationwide.  Out of our Atlanta location, we service customers from Miami to New York and from coast to coast.  If you live anywhere in the south-east region, we want to be your dealer.  We look forward to your visiting RPM Motorsports of Atlanta!</p>
*/
?>