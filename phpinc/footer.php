				</div><!-- contentHomeBody -->
				<img src="external/images/contentBot.png" alt="" class="contentBotIMG" /> 
			</div><!-- contentHome -->
		</div><!-- rightCol -->
		<div class="clear0"><!-- clear --></div>
		<div class="footer">
			<div class="footerLineOUT">
				<div class="footerLineIN">
					<div class="footerContent">
					<h3>&#169; 2008 RPM Motorsports of Atlanta</h3> 
					<img src="external/images/bulletspacer.png" alt="" />
					3097 Presidential Drive  Suite B   Atlanta, Georgia 30340
					<img src="external/images/bulletspacer.png" alt="" />
					770-216-2999
					<img src="external/images/bulletspacer.png" alt="" />
					<a href="?page=directions">Directions</a>
					<div id="credits">Developed by <a href="http://www.keenwebconcepts.com/" title="Visit Keen Innovations for your development needs">Keen Web Concepts</a>.  Design by <a href="http://www.modernwise.com/" title="Visit Modernwise Media Solutions Website">ModernWise Media Solutions</a></div>
					</div><!-- footerContent -->
				</div><!-- footerLineIN -->
			</div><!-- footerLineOUT -->
		</div><!-- footer -->
	</div><!-- container -->
</div><!-- outerContainer -->

<!--Google Analytics-->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3813278-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
<?php //wait for the window to load, and then adjust the bg image column fix ?>
<script type="text/javascript">
    $(window).load(function () {
        $('div.leftCol').css('min-height',$('div.rightCol').height());
    });
</script>
</body>
</html>