<?php 
//CR#1 SOLD!
$isSold = 0 != $data['sold'];
$counter = 0;

if ($isSold){ 
    echo '<div class="xfader-sold">'; #used for relative positioning for sold image 
}
?>
    <div class="xfader">
	    <?php
	    while($img = mysql_fetch_array($imgsResult)){ 
	        $counter ++;
  	      echo getCarImageByFileName($img['image_location'],$carString);
  	  }
  	  #since we used a while construct to loop through the results when we built this, we need to account for the new style (multiple photo uploads - & no default picture required) not having any images. 
  	  if (0 == $counter) { 
  	      echo getCarImgComingSoonTag();
        }
      ?>
    </div>

<?php
if ($isSold){
    printSoldImg(); 
    echo '</div>'; #xfader-sold
}
?>

<table id="showCaseCar">
	<tr>
		<td>
			Make:
		</td>
		<td>
			<?php echo $data['make']; ?>
		</td>
	</tr>
	<tr>
		<td>
			Model:
		</td>
		<td>
			<?php echo $data['model']; ?>
		</td>
	</tr>
	<tr>
		<td>
			Milage:
		</td>
		<td>
			<?php echo number_format($data['milage']); ?>
		</td>
	</tr>
	<tr>
		<td>
			Price:
		</td>
		<td>
			<?php echo '$' . number_format($data['price']); ?>
		</td>
	</tr>
</table>

<p class="description">
  <?php echo stripslashes($data['description']); ?>
</p>