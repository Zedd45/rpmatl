<?php
include_once "phpinc/headerSwitch.php";
include_once 'phpinc/globalUtils.php';
?>
</head>
<body>
	<div class="outerContainer">
		<div class="utilityNav">
			<?php include 'phpinc/fragments/utilityNav.inc'; ?>
		</div><!--utilityNav-->
		
		<div class="clear"><!-- clear --></div>
		
		<div class="container">
			<div class="leftCol">
				<div class="logo"><a href="?page=home"><img src="external/images/logo.png" alt="<?php echo $sitename ?>" /></a></div><!-- logo -->
				<div class="leftContentLeftLineOUT">
					<div class="leftContentLeftLineIN">
					<div class="clear"><!-- clear --></div>

						<div class="leftContent">
<?php
//for the <h3>, we set a variable so it prints the correct text, then call the logic that prints the divs for the cars with the images included.
$newest = true;
$carResult = getCarDataForThumbs(5);
include_once "phpinc/printCars.php";
?>
						</div><!-- leftContent -->
					</div><!-- leftContentLeftLineIN -->
				</div><!-- leftContentLeftLineOUT -->
			</div><!-- leftCol -->
			<div class="rightCol">
				<div class="nav">
				<ul>
					<?php
					# use PHP to determine the selected state - if you are on the page, use the active state instead
					?>
					<li><a href="?page=home" title="View our Homepage"><img src="external/images/nav_home_<?php $page=='home' ? print "active" : print "off"; ?>.jpg" alt="Home" onmouseover="this.src='external/images/nav_home_on.jpg'" onmouseout="this.src='external/images/nav_home_<?php $page=='home' ? print "active" : print "off"; ?>.jpg'" /></a></li>
					<li><a href="?page=inventory" title="Browse our Inventory"><img src="external/images/nav_inventory_<?php $page=='inventory' ? print "active" : print "off"; ?>.jpg" alt="Browse our Inventory" onmouseover="this.src='external/images/nav_inventory_on.jpg'" onmouseout="this.src='external/images/nav_inventory_<?php $page=='inventory' ? print "active" : print "off"; ?>.jpg'" /></a></li>
					<li><a href="?page=testimonials" title="View Testimonials"><img src="external/images/nav_testimonials_<?php $page=='testimonials' ? print "active" : print "off"; ?>.jpg" alt="View Testimonials" onmouseover="this.src='external/images/nav_testimonials_on.jpg'" onmouseout="this.src='external/images/nav_testimonials_<?php $page=='testimonials' ? print "active" : print "off"; ?>.jpg'"  /></a></li>
					<li><a href="?page=contact&amp;action=view" title="Send us a message"><img src="external/images/nav_contact_<?php $page=='contact' ? print "active" : print "off"; ?>.jpg" alt="Contact Us" onmouseover="this.src='external/images/nav_contact_on.jpg'" onmouseout="this.src='external/images/nav_contact_<?php $page=='contact' ? print "active" : print "off"; ?>.jpg'"  /></a></li>
					<!-- This is the far right corner of the main navigation - it creats the rounded edge -->
					<li><img src="external/images/nav_cap.jpg" alt="" class="navCap" /></li> 
				</ul>
			</div><!-- nav -->
			
<?php if ($page=='home' || $page=='homeTest'){ //only include the home promo on the homepage ?>
			
			<div class="promoHome">
				<img src="external/images/promo_home.jpg" alt="Advertisement" />
			</div><!-- promoHome -->

<?php }//end homepage switch ?>

			<div class="contentHome">
				<img src="external/images/contentTop.png" alt="" class="contentTopIMG" /> 
<?php /*  LEGACY CODE: 
if ($page=='home'){ //exclude images from non-home pages ?>
				<img src="external/images/hed_findmynextcar.png" alt="Find My Next Car" class="homeHed" />
<?php }//end homepage exclude  */?>
					<div class="contentHomeBody">
<?php
#page content will be pulled into this container by index.php
?>