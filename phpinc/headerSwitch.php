<?php
/** INVENTORY **/
if ($page=='inventory'){
?>
<style type="text/css">
	.contentHomeBody {
		padding:10px 5px;
	}
</style>
<?php 
}//end inventory switch

/** SHOWCASE **/
if ($page=='showcase'){ ?>
<script type="text/javascript" src="external/js/xfade.js"></script>
<script type="text/javascript">
	$(function(){
		$('.xfader').xfade({pause:1500,fadeTime:2500,fadeIn:true});
	})
</script>
<?php
}//end showcase switch

# at the current time, we use an 'action' GET variable on the forms - detect this, and throw some JQuery magic in there to change the field focus

if(isset($_GET['action'])){
	?>
	<!-- use Jquery to bind the focus and blur events to new functions that set a class so the EU can see which field s/he has selected -->
	<script type="text/javascript">
		$(function(){
			$('.formField')
					.focus(function(){$(this).addClass("formFieldSelected").removeClass("formField");})
					.blur(function(){$(this).addClass("formField").removeClass("formFieldSelected");});
		});
	</script>
	<?php
}

/** OTHER PAGES GO BELOW THIS LINE **/


/* LEGACY CODE!

    /*
    //Removed 3/08 - no search func. required (at most 10 - 15 cars online now)
    <style type="text/css">
    	#options { 
    		display:none;
    		margin:10px;
    		padding:10px;
    		color:#000;
    		background-color:#fff;		
    		}
    </style>

    <script type="text/javascript">
    	var toggle = 0;

    	function toggleOptions(){
    		toggle++;

    		if ( (toggle % 2) != 0){
    			$('#options').slideDown('normal'); 
    			$('a.showMore').html('Hide Options');
    		}
    		else {
    			$('#options').slideUp('normal'); 
    			$('a.showMore').html('Show More Options');
    		}
    	}
    </script>
*/
?>