<?php
	//print the results of the queries
	while($row = mysql_fetch_array($carResult)) {
		$carString = $row['caryear'].' '.$row['make'].' '.$row['model'];
		$car_id     = $row['car_id'];
		
		//CR#1 - SOLD!
		$isSold    = $row['sold'] != 0;
		$i=0;
		
		$headerText = $newest ? 'New Arrivals' : $carString;
		
		$wrapperClass =  $newest ?  'left' : 'right'; 
		$wrapperClass .= 'Profile';
		
		$imgContainer = $newest ? 'thumb' : 'main';
		
		//if this is the left Column, you will have been passed "newest"; otherwise, print all the right column stuff (inventory page)
	?> 
	
	<div class="<?php echo $wrapperClass; ?>">
		<h3><?php echo $headerText; ?></h3>
		
		<div class="rpm-<?php echo $imgContainer ?>-image">
	  	<a href="?page=showcase&amp;carid=<?php echo $car_id ?>" title="<?php echo 'Show more details for ' . $carString ?>">
  			<?php
  			if ($newest){
          echo getCarThumbImage($car_id,$row['imgName'],$carString);
  			}
  			else{ 
  			  // if this is the inventory page, you should be using larger images... so update a call for each car to grab the image.
  			  echo getCarImageById($car_id,$carString);
        }
  			//CR#1 - SOLD!
  			if ($isSold){ printSoldImg(); }
  			?> 		
  		</a>
		</div>
		
		<div class="<?php $newest ? print "left" : print "right" ?>ProfileDescription">
			<a href="?page=showcase&amp;carid=<?php echo $car_id ?>" title="<?php echo 'Show more details for ' . $carString ?>"><?php echo $carString ?></a>
				<?php echo number_format($row['milage']); ?> miles
				<br/><?php echo '$' . number_format($row['price']); ?> 
		</div><?php # End ProfileDescription ?>							
	</div><?php #end Profile ?>
	
<?php
	}//close while loop
?>