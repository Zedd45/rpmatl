<?php
/*
Uploadify v2.0.2
Release Date: July 29, 2009

Copyright (c) 2009 Ronnie Garcia, Travis Nickels

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

require_once "../phpcfg/config.php";
require_once '../phpcfg/dbCfg.php';
include_once '../phpObjects/SimpleImage.php';
include_once 'admin.inc.php';

connectRPM();
$imagePath = $DEV ? TESTIMGPATH : IMGPATH;

if (!empty($_FILES)) {
  $SI_handler = new SimpleImage();
  $car_id = clean($_REQUEST['carId']); //from uploadify scriptData property
  $image_name = null;
	
	$tempFile = $_FILES['Filedata']['tmp_name'];
	#this is safer to specify in this script, rather than in JavaScript: http://www.uploadify.com/documentation/options/folder/
  // $targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';  
  $targetPath = $_SERVER['DOCUMENT_ROOT'] . $imagePath;
  $targetFile =  str_replace('//','/',$targetPath) . $_FILES['Filedata']['name'];

	$fileTypes  = str_replace('*.','',$_REQUEST['fileext']);
	$fileTypes  = str_replace(';','|',$fileTypes);
	$typesArray = split('\|',$fileTypes);
	$fileParts  = pathinfo($_FILES['Filedata']['name']);
	
	#check that we're not overwriting an existing image
	while (file_exists($targetFile)){
	  $targetFile = str_replace('//','/',$targetPath) .  $counter . $_FILES['Filedata']['name'];
	  $counter++;
	}
	 
  if (in_array($fileParts['extension'],$typesArray)) {
    // Uncomment the following line if you want to make the directory if it doesn't exist
    // mkdir(str_replace('//','/',$targetPath), 0755, true);
    
    $success  = move_uploaded_file($tempFile,$targetFile);
    $outputMessage = $success ? "image upload Successful." : "image upload failed.";
    $image_name = clean(basename($targetFile));
    
    if ($success){
       $SI_handler->load($targetFile);
       $SI_handler->resizeToWidth(MAIN_IMG_WIDTH);
       $SI_handler->save($targetFile);
    }
		
    // $query = "INSERT INTO car_img SET car_id='$car_id',image_location='$image_name';";
    $query = "INSERT INTO car_img SET car_id='".$car_id."',image_location='".$image_name."';";		
    $result = mysql_query($query);
    $outputMessage .= false === $result ? " Image <b>NOT</b> added to database." : " Image added to database.";
		
    if ($DEBUG){
      echo "targetPath: $targetPath\n, targetFile: $targetFile\n, car_id: $car_id\n, Img name: $image_name\n, Query: $query \n\n.  Query returned: $result";
    }

    $outputMessage .= " Image resized. <p class=\"rpm-main-image\"><img src=\"" . TESTIMGPATH . $image_name . "\" class=\"rpm-preview-image\" /></p>";
		echo $outputMessage;
		
		#now, we need to check that the thumbnail has been created, and do so if not.
    // $query = "SELECT imgName FROM cars WHERE car_id = $car_id and imgName = '' LIMIT 1";    
    //     $result = mysql_query($query);
    //     if ($result){
    //       $thumb = getNewImageName($targetFile);
    //       $thumb_name = basename($thumb);
    //       
    //       $SI_handler->load($targetFile);
    //       $SI_handler->resizeToWidth(THUMB_IMG_WIDTH);
    //       $SI_handler->save($thumb);
    // 
    //       $outputMessage .= " Generated Thumbnail for image.";
    // 
    //       $thumb_query = "UPDATE cars SET imgName= $thumb_name WHERE car_id = $car_id LIMIT 1;";
    // 
    //       if ($DEBUG){
    //         echo "thumb Query is: $thumb_query";
    //       }
    // 
    //       $thumb_result = mysql_query($query);
    //       $outputMessage .= $thumb_result ? " Added Thumbnail image to database." : " <b>FAILED</b> to add thumbnail to database.  Please try uploading the file again.";
    //     }
    
    
  } else {
    echo 'Invalid file type.';
  }
}
?>