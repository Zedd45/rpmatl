<?php

if (isset($_POST['submit'])) { //Add the new car!
	
	$query = "INSERT INTO cars SET ";
		

	if ($price = getCleanNumber($_POST['price'])) {
		$query = $query . " price='" . $price . "', ";
	} else {
		echo "<b>Did NOT add price because it is not a valid number: " . $_POST['price'] . "</b><br/>";
	}
	if ($year = getCleanNumber($_POST['year'])) {
		$query = $query . " caryear='" . $year . "', ";
	} else {
		echo "<b>Did NOT add year because it is not a valid number: " . $_POST['year'] . "</b><br/>";
	}
	if ($milage = getCleanNumber($_POST['milage'])) {
		$query = $query . " milage='" . $milage . "', ";
	} else {
		echo "<b>Did NOT add milage because it is not a valid number: " . $_POST['milage'] . "</b><br/>";
	}
  // $query = $query . " imgName='" .   $thumb . "', " .
  $query .= 
		"description='" .	mysql_real_escape_string($_POST['description']) .  "', " .
		"make_id='" .		$_POST['make_id']. "', " .
		"model_id='" .		$_POST['model_id']. "'; ";
	
	//echo $query . "<br/>";
	$result = mysql_query($query);
	
	if ($result) { 
		$car_id_query = "SELECT car_id FROM cars ORDER BY date_submitted DESC LIMIT 1";
		$car_id_result = mysql_query($car_id_query);
		if ($car_id_result) $car_id = mysql_fetch_row($car_id_result);
	?>
    	<p><b>The car was Added!</b></p>
	    <p>
	      Please <a href="/phpAdminPanel/?page=editCar&amp;id=<?php echo $car_id[0]; ?>">Edit</a> the car to add photos.
	    </p>
	<?php 
	 } else {
		echo "<p><b>There was a DataBase error adding this car.</b><br/></p>";
		echo $query . "<br/>";
		$report_success = error_log("error executing query: $query.  MySQL Error: " . mysql_error(), 1,WEBMASTER);
		if ($report_success) { 
			echo "<p>An email was sent to your webmaster...</p>";
		}
	}
}

$makes = getMakes();
$models = getModels();

#prior to 01 '11, we allowed a single photo to be uploaded with each car, and created the thumb & main image from that, instead of the multiple uploader.
/* 
<form method=post action="?page=addCar" enctype="multipart/form-data">
<input type="hidden" name="MAX_FILE_SIZE" value="7340000000" />
*/
?>

<form method=post action="?page=addCar">
<table>
	<tr>
		<td>
Make:
		</td>
		<td>
<select name="make_id">
	<?php 
	foreach($makes as $make_id => $make) {
    echo "<option value=\"$make_id\">$make</option>>";
	}
	?>
</select>
		</td>
	</tr>
	<tr>
		<td>
Model:
		</td>
		<td>
<select name="model_id">
	<?php 
	foreach($models as $model_id => $model) {
		echo "<option value=\"$model_id\">$model</option>";
	}
	?>
</select>
		</td>
	</tr>
<?php 
/*
	<tr>
		<td>
Picture: 
		</td>
		<td>
		<input name="picture" type="file" />
		</td>
	</tr>
*/
?>
	<tr>
		<td>
Year: 
		</td>
		<td>
			<input type="text" name="year" value=""/><br/>
		</td>
	</tr>
	<tr>
		<td>
Description:
		</td>
		<td>
			<textarea name="description" cols="30" rows="5"></textarea>
		</td>
	</tr>
	<tr>
		<td>
Price: 
		</td>
		<td>
			<input type="text" name="price" value=""/>
		</td>
	</tr>
	<tr>
		<td>
Milage: 
		</td>
		<td>
			<input type="text" name="milage" value=""/>
		</td>
	</tr>
	
</table>
<input type="submit" name="submit" value="Add This Car"/>
</form>



<?php

#### LEGACY  ######

/*
$name = '';
if (isset($_FILES['picture']) && $_FILES['picture']['name'] != '') {
	$local_name =  "../external/images/" . basename($_FILES['picture']['name']);
	$name = basename($_FILES['picture']['name']);
	for ($i=0; file_exists($local_name); $i++) {
		$name = $i . basename($_FILES['picture']['name']);
		$local_name = "../external/images/" . $name;
	}
	if (move_uploaded_file($_FILES['picture']['tmp_name'], $local_name)) {
		// Successfully uploaded and moved.
		// Get new sizes
		list($width, $height) = getimagesize($local_name);
		$newwidth = "181px";
		$newheight = "135px;";

		// Load
		$thumb = imagecreatetruecolor($newwidth, $newheight);
		$source = imagecreatefromjpeg($local_name);

		// Resize
		imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

		// Output
		
		imagejpeg($thumb, "../external/images/thumb_" . $name);
		unlink("../external/images/" . $name);
		$name = "thumb_" . $name;
	} else {
		echo "<b>New Image '" . $_FILES['picture']['name'] . "' was NOT uploaded successfully.</b> ". $_FILES['picture']['error']."<br/>";
		$name = '';
	}
} else {
	echo "<b>No new image added</b><br/>";
}
$thumb = '';
if (isset($_FILES['picture']) && $_FILES['picture']['name'] != '') {
	$thumb = moveUploadedImage($_FILES['picture'], 181, 135);
	$imgName = moveUploadedImage($_FILES['picture'], 640, 480);
	if (!$thumb || !$imgName) {
		echo "<b>New Image '" . $_FILES['picture']['name'] . "' was NOT uploaded successfully.</b> ". $_FILES['picture']['error']."<br/>";
	}
	
} else {
	echo "<b>No new image added</b><br/>";
}



if ($result) {
if (isset($imgName)) {
	$query = "SELECT car_id FROM cars ORDER BY car_ID DESC LIMIT 1;";
	$result = mysql_query($query);
	if ($row = mysql_fetch_array($result)) {
		$query = "INSERT INTO car_img SET car_id='" . $row['car_id'] . "', image_location='" . $imgName . "';";
		$result = mysql_query($query);
		if (!$result) {
			echo "<b>There was a DB error adding this car</b><br/>";
			echo $query . "<br/>"; 
			error_log("error executing query: $query", 1,WEBMASTER);
		}
	} else {
		echo "<b>There was a DB error adding this car</b><br/>";
		echo $query . "<br/>";
	}
}
//else goes here 
*/

?>