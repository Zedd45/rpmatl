<?php

if (!isset($_GET['id'])) {
	header("Location: ?page=listCars");
	exit();
}

$car_id = $_GET['id'];

// Making changes
if (isset($_POST['submit'])) {
	$query = "UPDATE cars SET ";
	if ($price = getCleanNumber($_POST['price'])) {
		$query .= " price='" . $price . "', ";
	} else {
		echo "<b>Did NOT change price because it is not a valid number: " . $_POST['price'] . "</b><br/>";
	}
	if ($year = getCleanNumber($_POST['year'])) {
		$query .= " caryear='" . $year . "', ";
	} else {
		echo "<b>Did NOT change year because it is not a valid number: " . $_POST['year'] . "</b><br/>";
	}
	if ($milage = getCleanNumber($_POST['milage'])) {
		$query .= " milage='" . $milage . "', ";
	} else {
		echo "<b>Did NOT change milage because it is not a valid number: " . $_POST['milage'] . "</b><br/>";
	}
	//CR#1: SOLD Indicator - if the checkbox is in the $_POST array it means that it was set.  Change the car to sold.
	if (isSet($_POST['sold'])){
	    $query .= " sold=1, ";
	    echo '<p style="color:red;"><strong>Vehicle currently listed as &quot;SOLD!&quot;</strong><p>';
	} else {
	    $query .= " sold=0, ";
	}
	//END CR#1
	$query .= "description='" .	mysql_real_escape_string($_POST['description']) . "', " .
	
		"make_id='" .		$_POST['make_id']. "', " .
		"model_id='" .		$_POST['model_id']. "' " .
	
		" WHERE car_id='$car_id' LIMIT 1;";
		
	if ($DEBUG) {
        echo $query . "<br />";
    }

    //process the result / update the DB
	$result = mysql_query($query);
	
	//print success/fail message
	if ($result){
		echo "<b>Changes committed successfully!</b><br/>";
	}
	else {
		echo "<b>Changes were NOT COMMITTED!</b><br/>";
	}
	    
    
	    		
		
	// Check to see if a new picture was uploaded
	// Similar code in addCar (This should be a shared function), but I am being lazy.
	if (isset($_FILES['picture']) && $_FILES['picture']['name'] != '') {
		$image_location = moveUploadedImage($_FILES['picture'], 640, 480);
		if (!$image_location) {
			echo "<b>New Image '" . $_FILES['picture']['name'] . "' was NOT uploaded successfully.</b> ". $_FILES['picture']['error']."<br/>";
		} 
		else {
			$query = "INSERT INTO car_img SET car_id='".$car_id."',image_location='".$image_location."';";
			$result = mysql_query($query);
			if ($result)
				echo "<b>New Image '" . $_FILES['picture']['name'] . "' was uploaded and committed successfully.</b><br/>";
			else
				echo "<b>New Image '" . $_FILES['picture']['name'] . "' was uploaded successfully, but it was not committed.</b><br/>";
		}
	} 
	else {
		echo "<b>No new image added</b><br/>";
	}
	
		
}

$makes = getMakes();

$models = getModels();

// Get car information
$query = "SELECT * FROM cars WHERE car_id='$car_id';";
$result = mysql_query($query);
$row = mysql_fetch_array($result);

//CR#1 - SOLD
$isSold = $row['sold'] != 0;

?>

<p>
    <a href="?page=multipleImageUploader&id=<?php echo $car_id ?>">Upload Multiple Images for this Vehicle</a>
    <br/>(prototype)
</p>

<p><a href="?page=deleteCar&amp;car_id=<? echo $car_id; ?>">Delete This Vehicle</a></p>

Thumbnail:<br/>
<div class="thumb">
    <img src="../external/images/<?php echo $row['imgName']; ?>" /><br/><br/>
    <?php if($isSold){
        printSoldImg();
    } ?>
</div>

<form method=post action="<? echo "?page=editCar&amp;id=$car_id"; ?>" enctype="multipart/form-data">
	<input type="hidden" name="MAX_FILE_SIZE" value="7340000000" />
	<!-- Name of input element determines name in $_FILES array -->
	<table>
		<tr>
			<td>
	Add a Picture:
			</td>
			<td>
				<input name="picture" type="file" />
			</td>
		</tr>
		<tr>
			<td>
	Make:
			</td>
			<td>
	<select name="make_id">
		<? foreach($makes as $make_id => $make) {
			?><option <? if($make_id == $row['make_id']) echo "selected='selected'"; ?> value="<? echo $make_id; ?>"><? echo $make; ?></option>><?
			}
		?>
	</select>
			</td>
		</tr>
		<tr>
			<td>
	Model:
			</td>
			<td>
	<select name="model_id">
		<? foreach($models as $model_id => $model) {
			?><option <? if($model_id == $row['model_id']) echo "selected='selected'"; ?> value="<? echo $model_id; ?>"><? echo $model; ?></option>><?
			}
		?>
	</select>
			</td>
		</tr>
		<tr>
			<td>
	Year: 
			</td>
			<td>
				<input type="text" name="year" value="<? echo $row['caryear']; ?>"/>
			</td>
		</tr>
		<tr>
			<td>
	Description:
			</td>
			<td>
				<textarea name="description" cols="30" rows="5"><? echo $row['description']; ?></textarea>
			</td>
			<td>
		</tr>
		<tr>
			<td>
	Price: 
			</td>
			<td>
				<input type="text" name="price" value="<? echo $row['price']; ?>"/>
			</td>
		</tr>
		<tr>
			<td>
	Milage: 
			</td>
			<td>
				<input type="text" name="milage" value="<? echo $row['milage']; ?>"/>
			</td>
		</tr>
		<tr>
		    <td>
	Sold:    
		    </td>
		    <td>
		        <input type="checkbox" name="sold" <?php $isSold ? print('checked="checked"') : print(''); ?>/>
		    </td>
		</tr>
	</table>
	<input type="submit" name="submit" value="Make Changes To Car"/>
</form>

<?
// Get extra images for car
// Don't smash up $row
$query = "SELECT img_id,image_location FROM car_img WHERE car_id='$car_id' ORDER BY img_id;";
$result = mysql_query($query);

if ($DEBUG) {
    echo $query;
}

$first_img = true;
while ($img = mysql_fetch_array($result)) {
	
	if ($first_img && $isSold){
	    echo '<div class="large">';
		  printSoldImg();
	}
	
	echo "<img src=\"../external/images/".$img['image_location']."\"/><br/>";
	
	if (!$first_img){
		echo "[<a href=\"?page=deleteImg&amp;img_id=".$img['img_id']."&car_id=".$car_id."&image_location=".$img['image_location']."\">Delete above image</a>]<br/>";
	}
	else {
	    $first_img = false;
	    if ($isSold) { echo '</div>'; }
	}
}



/* LEGACY CODE!

	if (isset($_FILES['picture']) && $_FILES['picture']['name'] != '') {
		$local_name =  "../external/images/" . basename($_FILES['picture']['name']);
		$name = basename($_FILES['picture']['name']);
		for ($i=0; file_exists($local_name); $i++) {
			$name = $i . basename($_FILES['picture']['name']);
			$local_name = "../external/images/" . $name;
		}
		if (move_uploaded_file($_FILES['picture']['tmp_name'], $local_name)) {
			// Successfully uploaded and moved.
			// Committ to the DB
			$query = "INSERT INTO car_img SET car_id='".$car_id."',image_location='".$name."';";
			$result = mysql_query($query);
			if ($result)
				echo "<b>New Image '" . $_FILES['picture']['name'] . "' was uploaded and committed successfully.</b><br/>";
			else
				echo "<b>New Image '" . $_FILES['picture']['name'] . "' was uploaded successfully, but it was not committed.</b><br/>";
		} else {
			echo "<b>New Image '" . $_FILES['picture']['name'] . "' was NOT uploaded successfully.</b> ". $_FILES['picture']['error']."<br/>";
		}
	} else {
		echo "<b>No new image added</b><br/>";	
	}
	*/