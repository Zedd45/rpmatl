<?php
require_once '../phpinc/globalUtils.php';
require_once '../phpcfg/config.php';
require_once '../phpcfg/dbCfg.php';

function checkIt($postName){
	if (isset($_POST[$postName]) && strlen(trim($_POST[$postName])) > 0 )
		return true;
	return false;
}

/**
 * method to use when outputting user POSTS data to the browser.  Currently supports Edit Testimonial
 * cleans input of tags, leading & trailing whitespace and slashes, if magic quotes is on
 * @param $variable - the name of the variable in the global request you want to sanitize. 
 * @return <type> the sanitized value.
 */
function sanitize($variable){
	$str = strip_tags(trim($_POST[$variable]));
	if(get_magic_quotes_gpc()) {
		$str = stripslashes($str);
	}
	return $str;
}

//Function to sanitize values received from the form. Prevents SQL injection.  alternative to cleanIt
function clean($str) {
	$str = trim($str);
	if(get_magic_quotes_gpc()) {
		$str = stripslashes($str);
	}
	return mysql_real_escape_string($str);
}

function getMakes() {
	// Get an array of all the possible makes
	$query = "SELECT make,make_id FROM make ORDER BY make;";
	$result = mysql_query($query);
	while ($row = mysql_fetch_array($result)) {
		$makes[$row['make_id']] = $row['make'];
	}
	return $makes;
}

function getModels() {
	// Get an array of all the possible models
	$query = "SELECT model,model_id FROM model ORDER BY model;";
	$result = mysql_query($query);
	while ($row = mysql_fetch_array($result)) {
		$models[$row['model_id']] = $row['model'];
	}
	return $models;
}

//replace commas and dollarsigns in the textfield with empty strings, then check that the value is numeric before inserting into the DB.
function getCleanNumber($number) {
	$number = str_replace("$", "", $number);
	$number = str_replace(",","", $number);
	if (is_numeric($number))
		return $number;
	return false;
}

/* LEGACY: This currently supports testimonials. */

/** 
* moves the image to the ../external/images/ folder on the server, the path to which is pulled back from the DB
* @NOTE:DEPRICATED.  use SimpleImage.php
* @requires GD Library
* @param $file = The $_FILES array
* @return  false on error; new file name on success
*/
function moveUploadedImage($file, $newwidth, $newheight) {
	$name = '';
	
	if (isset($file) && $file['name'] != '') {
    
    #check that we're not overwriting an existing image.  	
		$local_name =  "../external/images/" . basename($file['name']);
		$name = basename($file['name']);
		for ($i=0; file_exists($local_name); $i++) {
			$name = $i . basename($file['name']);
			$local_name = "../external/images/" . $name;
		}
		
		if (is_uploaded_file($file['tmp_name'])) {
			// Successfully uploaded and moved.
			// Get new sizes
			list($width, $height) = getimagesize($file['tmp_name']);

			// Process through GD library 
			if (!($thumb = imagecreatetruecolor($newwidth, $newheight))) return false;
			if (!($source = imagecreatefromjpeg($file['tmp_name']))) return false;

			// Resize
			if (!imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height)) return false;
			imagedestroy($source);
			// Output
			
			imagejpeg($thumb, $local_name);
			imagedestroy($thumb);
			return $name;
		}
		throw new Exception("File could not be resized!");
	}
	throw new Exception("unable to locate file!");
}

function uploadCarImg($inputIndex=0,$inputId='picture'){
  connectRPM();
    $imgToUpload = $_FILES[$inputId]['name'][$inputIndex];

    echo "<p>img: $imgToUpload</p>";

    if ($imgToUpload != ''){
    	$image_location = moveUploadedImage($imgToUpload, 640, 480);
    	if (!$image_location) {
    		echo "<b>New Image '" . $imgToUpload . "' was NOT uploaded successfully.</b> ". $_FILES[$inputId][$inputIndex]['error']."<br/>";
    	} 
    	else {
    		$query = "INSERT INTO car_img SET car_id='".$car_id."',image_location='".$image_location."';";
    		$result = mysql_query($query);
    		if ($result)
    			echo "<b>New Image '" . $imgToUpload . "' was uploaded and committed successfully.</b><br/>";
    		else
    			echo "<b>New Image '" . $imgToUpload . "' was uploaded successfully, but it was not committed.</b><br/>";
    	}
    } 
    else {
    	echo "<b>No new images added</b><br/>";
    }
}

#Never really got this to work....

/*
 * if the file exists, add a digit before the filename to create a new file for it
 * this allows the Admin User to upload multiple DSC001.JPGs (if (s)he clears the memory card, for example), without us having to write complicated logic to confirm duplicates.
 * @param $fileName string - file name of image with no path (ie. picture.png)
 * @return $newFileName - returns either the original string if the file did not already exist on the server, a new filename, bereft of path info if it did
 * NOTE: this was originally done on legacy due to time constraints, but is a valid solution to a complex problem, although not nearly a perfect one.
 * 
function getValidImageFileName($fileName){
  $path = $DEV ? TESTPATH : IMGPATH;
  $newFileName = $fileName;
  $counter = 0;
	while (file_exists($path . $newFileName)) {
		$newFileName =  $counter . $fileName;
		$counter++;
	}
	return $newFileName;
}
*/
?>