<?php 

session_start();
connectRPM();
if (!isSet($_SESSION['loggedIn'])){

    //check that we received a user name & password
    if (isset($_POST['usr']) && isset($_POST['pwd']) ) {
        $usr = clean($_POST['usr']);
        $pwd = clean($_POST['pwd']);


    	$userQuery = "SELECT user_id FROM admin WHERE user_name = '$usr' and password=PASSWORD('$pwd') LIMIT 1";
    	$userResult = mysql_query($userQuery);
    	$row = mysql_fetch_array($userResult);

    	//if any rows are returned, log in!
    	if($row){
    		$_SESSION['loggedin']=true;
    	}

    	unset($usr);
    	unset($pwd);
    }
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>RPMATL Admin Panel v1.1</title>
	<link rel="stylesheet" href="../external/css/adminStyle.css" media="all"/>
</head>
<body>
<?php 

if (isSet($_GET['logout'])){
    ?>
    <?php
    if(isSet($_SESSION['loggedIn'])){
        $_SESSION['loggedIn'] = false;
    }
    session_unset();
    session_destroy();
    ?>
    <script type="text/javascript">
        function replaceLoc(){
            window.location.replace("http://www.rpmatl.com/");
        }
        
        setTimeout(replaceLoc,2500);
    </script>
    <?php
    echo "<p>You logged out successfully. Please close the window and / or exit the browser to ensure data integrity.  Thank you.</p>";
    exit();
}

if (isSet($_SESSION['loggedin'])){ ?>
    <a href="index.php?logout=true">Logout</a> 
<?php 
} else { ?>
    <form action="" method="post" name="login" id="auth">
        <label>User:</label><input type="text" name="usr" value=""/>
        <br/>
        <label>Pass:</label><input type="password" name="pwd" value=""/>
        <input type="submit" name="submit"/>
    </form> 
<?php  
//halt further scripts if not logged in!
exit();
}
?>