<?php

//if we can't find an id for the item to edit, redirect to the landing page
if (!isset($_GET['id'])) {
	header("Location: ?page=listTestimonials");
	exit();
}

$t_id = mysql_real_escape_string($_GET['id']);

/**
*      This is where we process the form.
*
*	If submitted is found in the $_POST array, the form was submitted, and we can work with the data to make update queries.
*   Either way, we need to pull the initial query, as we need to display the results of the update to our admin user.
**/

if (isset($_POST['submitted'])){
	//set the defaults to the empty string - we'll check for this later.
	$caption = '';
	$imgPath = '';
	
	
	//start building the query
	$update = "UPDATE testimonials SET ";
	
	//grab the form fields, if they exist and are not the empty string
	if(checkIt('caption')){
		$caption = sanitize('caption');
		$update .= " data = '$caption' ";
	}
	
	/* we will finish this later - it's not updating the image path and we need to delete the existing image first, anyway
	//if the $_FILES superglobal is set, we should upload the picture and set the path.
	if (isset($_FILES['picture']) && $_FILES['picture']['name'] != '') {
		$image_location = moveUploadedImage($_FILES['picture'], 640, 480);
		if (!$image_location) {
			echo "<b>New Image '" . $_FILES['picture']['name'] . "' was NOT uploaded successfully.</b> ". $_FILES['picture']['error']."<br/>";
		} 
		//if image location was set, add it to the Query
		else {
			$query .= ", image_location='$image_location' ";	
			echo "New image created @ $image_location";
		}
	} 
	else {
		echo "<b>No new image added</b><br/>";
	}
	*/
	
	//finalize the update
	$update .= " WHERE testimonial_id = '$t_id' LIMIT 1;";


	mysql_query($update);
	  
}

//create a query to find the item we want to edit - the form is printed below, and we don't want to make this Query before the above Query has been executed.
$query  = "SELECT * FROM testimonials WHERE testimonial_id = '$t_id' LIMIT 1;";
$result = mysql_query($query);
$final  = mysql_fetch_array($result);

?>

	<img src="../external/images/<?php echo $final['image_location']; ?>"/>

	<form action="?page=editTestimonial&amp;id=<?php echo $t_id; ?>" method="post" name="edit" enctype="multipart/form-data">
	
		<?php 
		/* later
		Change the Picture?
		<br/><input name="picture" type="file" />
		
		<br/><br/>
		*/
		?>
		Change the Caption?
		<br/><textarea name="caption" cols="30" rows="5"><?php echo $final['data']; ?></textarea>
		
		<input type="hidden" name="MAX_FILE_SIZE" value="7340000000" />
		<input type="hidden" name="submitted" value="true"/>
	
		<br/><input type="submit" name="submit" value="Update"/>
	</form> 