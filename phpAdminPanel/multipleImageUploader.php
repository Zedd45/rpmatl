<?php
/**
 * CR#2: Multiple Image Uplaoder 
 * The theory behind this is to use jQuery uploadify plugin to allow Carlo to upload multiple images at one time, and resize each of them as Flash (uploadify.swf) calls the PHP script via AJAX: handleMultipleImageUpload.php
 */
$car_id = clean($_GET['id']);
?>
<script src="/external/js/jquery-1.3.1.min.js" type="text/javascript" charset="utf-8"></script>
<script src="/external/js/swfobject.js" type="text/javascript" charset="utf-8"></script>
<script src="/external/js/jquery.uploadify.v2.0.2_development.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
		$(document).ready(function() {
			$("#uploadify").uploadify({
				'uploader'			 : '/external/flash/uploadify.swf',
				'script'				 : 'handleMultipleImageUpload.php',
				'cancelImg'			 : '/external/images/cancel.png',
				'queueID'				 : 'fileQueue',
				'auto'					 : true,
				'multi'					 : true,
				'buttonText'		 : 'Upload Photos',
				'fileDesc'			 : '*.jpg;*.jpeg;*.png;*.JPG;*.JPEG;*.PNG',
				'fileExt'				 : '*.jpg;*.jpeg;*.png;*.JPG;*.JPEG;*.PNG',
				'scriptData'     : {'carId' : <?php echo $car_id; ?>},
				'onComplete'		 : function(event, ID, fileObj, response, data) {
            $('#rpm-uplodify-results').append(response);
              // console.log("method params: ",event, ID, fileObj, response, data);
              // console.log(response);
				},
				onError				   : function(event,ID,fileObj,errorObj) {
					if (window.console && "function" == typeof window.console.error) {
						console.error(errorObj.type + ' Error: ' + errorObj.info);
					}
					else {
					  alert("An Error Has occured: " + errorObj.type + ' Error: ' + errorObj.info);
					}
					//TODO: add ajax call for error_log here
				}
			});
		});
</script>

<p>[Take me back; I want to <a href="/phpAdminPanel/?page=editCar&id=<?php echo $car_id; ?>">Edit this car's information.</a>]</p>

<h3>Click the button below to begin.</h3>
<p>You can select multiple files by holding the control (ctrl) button or the shift button and clicking.</p>

<div>
		<!-- <p><a href="javascript:jQuery('#uploadify').uploadifyClearQueue()">Cancel All Uploads</a></p> -->
		<input type="file" name="uploadify" id="uploadify"/>
		<div id="fileQueue" style="margin:25px 0;"></div>
</div>

<div id="rpm-uplodify-results"> </div>

<?php 
/*
#This could would limit the number of uploads to the server, if PHP version >= 5.2.12, and the ini is set
$limitUploads = false;
$maxUploads = ini_get('max-file-uploads');
if (is_numeric($maxUploads)) {
	$limitUploads = true;
}

#add this bit at the top of the uploadify init block
	if ($limitUploads){
		echo "'queueSizeLimit' : $maxUploads,";
	}
*/
?>