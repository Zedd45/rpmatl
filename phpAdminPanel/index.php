<?php
# Start output buffering so we can send headers depending on the page (so we can send redirects).
ob_start();

require_once "../phpcfg/config.php";
require_once '../phpcfg/dbCfg.php';
require_once "adminHeader.php";
include_once "admin.inc.php";
include_once '../phpinc/globalUtils.php';

/* 
   if Service flag is set in /phpConfig/config.php, present the service message, otherwise:
   if the page is set, use it - otherwise, load the landing page text.
   if the page is set, but we can't find it, inform our user and ask them to go back.
*/
if (SERVICE){
    include('mx_msg.php');
}
else if ($page!='') {
	#print the utility nav
	?>
	<div style="margin:10px 0;">
		[<a href="?page=listCars">List of Cars</a>] [<a href="?page=addCar">Add a new Car</a>] [<a href="?page=addMake">Add a new Make</a>] [<a href="?page=addModel">Add a new Model</a>] 
		<br/>
		[<a href="?page=listTestimonials">List Testimonials</a>] [<a href="?page=addTestimonial">Add Testimonial</a>]
	</div>
	<?php 
	if (file_exists($_GET['page'].'.php')){
		include $_GET['page'].'.php';
	}
	else {
		?>
		<p>We could not find the page you specified.  Please <a href="javascript:history.go(-1);">go back</a></p>
		<p>If you feel you have reached this page in error, please contact your administrator</p>
		<?php
	}
}
//
else{
	?>
	<!-- center it for IE, then handle standards compliant browsers in the div beneath that -->
	<div style="text-align:center;">
		<div style="margin:50px auto; width:400px;">
			<h3>Where would you like to start?</h3>

			<a href="?page=listCars">Work with Inventory</a>
			| <a href="?page=listTestimonials">Work with Testimonials</a> 
		</div>
	</div>
	<?php 
}


ob_end_flush();
require_once "adminFooter.php";

//destroy variables in memory
unset($page);
unset($sitename);
unset($domain);
unset($DEBUG);
?>