<?php

require_once "../phpObjects/testimonial.php";

//create a new object so we can print all the values by calling the next call until there are no more rows in the DB.
$testimonials = new Testimonial();
$range = $testimonials->range();

/* set up the link line to pass the offset and limit to the object */
if (isset($_GET['offset']) && isset($_GET['limit']))
	$testimonials = new Testimonial($_GET['offset'], $_GET['limit']);
else
	$testimonials = new Testimonial();
$range = $testimonials->range();


?>
<p><? echo $testimonials->link_line("?page=listTestimonials"); ?></p>
<?php

while ($info = $testimonials->next() ) {    
	?>
	<hr/>
	<div>
		<img src="../external/images/<? echo $info['image_location']; ?>" /><br/>
		<div class="testimonialTxt"><? echo $info['data'] . "\n"; ?></div><!--testimonialTxt-->
		<div class="links">
			<a href="?page=editTestimonial&amp;id=<?php echo $info['testimonial_id']; ?>">Edit</a> |
			<a href="?page=deleteTestimonial&amp;id=<?php echo $info['testimonial_id']; ?>">Delete</a>
		</div><!--links-->
	</div>
	<?
}//close while loop for printing testimonials


?>
<p><? echo $testimonials->link_line("?page=listTestimonials"); ?></p>
<?php

//destroy the vars in memory by calling the destructor
$testimonials->__destruct();

?>

