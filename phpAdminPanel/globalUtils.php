<?php
require_once "../phpcfg/config.php"; 
require_once "../phpcfg/dbCfg.php";

##=======================VIEW HELPERS========================##

# CR#1 - SOLD
function printSoldImg(){
    echo '<img src="' . IMGPATH . 'sold_indicator.png" class="sold" alt="SOLD!">';
}

/**
 * returns a mysql result set for $count cars
 * @param $count - integer limit for cars to return.  false will return no limit.
 * @return mysql result
 */
function getCarDataForThumbs($count = 5){
  connectRPM();  
  $carSelector = mysql_real_escape_string("cars.imgName, cars.caryear, make.make, model.model, cars.price, cars.milage, cars.car_id, cars.sold");
  # make a connection to the database and print the newest cars by forming a query from the most recently updated entries to the DB (get the date_submitted)  
  $carOrder    = mysql_real_escape_string("cars.date_submitted DESC");
  $carWhere    = mysql_real_escape_string("cars.make_id = make.make_id AND cars.model_id = model.model_id");
  $carQuery    = "SELECT $carSelector FROM cars, make, model WHERE $carWhere ORDER BY $carOrder LIMIT $count;";
  printQuery($carQuery);
  $carResult   = mysql_query($carQuery);
  return $carResult;
}

/**
 * Returns the first large image (primarily for inventory page)
 * @param $car_id = the ID of the car in the DB
 * @param carString - the data to print for title & alt - preferably the car information
 * @return string image for car
 */
function getCarImageById($car_id,$carString = 'vehicle'){
  $image = getCarImageSQL($car_id);
	return getCarImageByFileName($image,$carString);
}

/**
 * @param $imageFileName = String for filename location.  
 * @param carString - the data to print for title & alt - preferably the car information
 */
function getCarImageByFileName($imageFileName,$carString = 'vehicle'){
  $imageLocation = getFullImagePathByFileName($imageFileName);
  if (false !==  $imageLocation){
      return getCarImageTag($imageLocation,$carString);
  }
  else {
    return getCarImgComingSoonTag();
  }
}

/**
 * checks if there is an entry for the thumbnail in the DB (legacy) or creates a thumb on the fly
 * @param $car_id = the ID of the car in the DB
 * @param $imageFileName = String for filename location.  
 * @param carString - the data to print for title & alt - preferably the car information
 * @return string image for car
 */
function getCarThumbImage($car_id, $imageFileName, $carString = 'vehicle') {
  $imageLocation = getFullImagePathByFileName($imageFileName);
  if (false !==  $imageLocation){ #if the image exists, this is the legacy solution - the thumb is on the file system
      return getCarImageTag($imageLocation,$carString);
  }
  else {
    return getThumbNailByCarId($car_id, $carString);
  }	
}

/**
 * fallback if there is no image in the database (not legacy)
 * @param $car_id = the ID of the car in the DB
 * @param carString - the data to print for title & alt - preferably the car information
 * @return string image for car
 */
function getThumbNailByCarId($car_id, $carString = 'vehicle'){
  $fileName =  getCarImageSQL($car_id);
  if ($fileName){
    $path =  "/utils/thumbnail.php?fileName=$fileName";
    return getCarImageTag($path,$carString);
  }
  else {
    return getCarImgComingSoonTag(true);
  }
}



##=======================FUNCTION HELPERS========================##

/**
 * preforms a SQL lookup in the car images table for the first image associated with a car ID
 * @param $car_id = the ID of the car in the DB
 * @return <type> String fileName - the image filename if found, NULL otherwise
 */
function getCarImageSQL($car_id){
  if (!$car_id) {return null;}
  $imgQuery  = "SELECT image_location FROM car_img WHERE car_id ='$car_id' ORDER BY img_id ASC LIMIT 1;";
	$imgResult = mysql_query($imgQuery);
	$imgPath = mysql_fetch_array($imgResult);
	return $imgPath['image_location'];
}

/**
 * @param $src - the src attr of the image tag
 * @param carString - the data to print for title & alt - preferably the car information
 * @return string for img tag
 * NOTE: the return should NOT be wrapped in a div on showcase, or it will break the xfader.
 */
function getCarImageTag($src,$carString){
  return "<img src=\"$src\" alt=\"$carString image\" title=\"$carString\" />";
}

/**
 * @param $thumb boolean - if true, return a smaller size
 * @return the string for the HTML IMG tag ~= "Photo Coming Soon" 
 */
function getCarImgComingSoonTag($thumb = false){
  $title = "Image not available.";
  $alt = "Photo Coming Soon.";
  if ($thumb){
    return "<img src=\"" . IMGPATH . "coming_soon_thumb.png\" title=\"$title\" alt=\"$alt\" />";
  }
  return "<img src=\"" . IMGPATH . "coming_soon.png\" title=\"$title\" alt=\"$alt\" />";
}

/**
 * debugging function for mysql queries
 * NOTE: doesn't seem to work at present
 */
function printQuery($query){
  if ($DEBUG){
    $string = "query preformed: $query";
    error_log($string, 1, WEBMASTER);
    echo "<script>console.log('$string')</script>";
  }
}

/**
 * function designed to calculate paths for testing image manipulation tools.
 * @param $fileName string - file name of image with no path (ie. picture.png)
 * @return $filePath string - the fully qualified path to the image. False if no image can be found on the server
 * NOTE: returns false if no image can be found on the server
 */
function getFullImagePathByFileName($fileName){
  $fileName = trim($fileName);
  $root = $_SERVER['DOCUMENT_ROOT'];
  $standardPath = IMGPATH . $fileName;
  $testPath = TESTIMGPATH . $fileName;
  
  if (null == $fileName || '' == $fileName) { 
     return false; 
  }
  else if (file_exists($root . $standardPath)){ 
    return $standardPath; 
  }
  else if (file_exists($root . $testPath)){ 
    return $testPath; 
  }
}

?>