<?php

require_once '../phpinc/globalUtils.php';

function checkIt($postName){
	if (isset($_POST[$postName]) && strlen(trim($_POST[$postName])) > 0 )
		return true;
	return false;
}

function cleanIt($postName){
	return mysql_real_escape_string(trim(strip_tags($_POST[$postName])));
}

//Function to sanitize values received from the form. Prevents SQL injection
function clean($str) {
	$str = @trim($str);
	if(get_magic_quotes_gpc()) {
		$str = stripslashes($str);
	}
	return mysql_real_escape_string($str);
}



function getMakes() {
	// Get an array of all the possible makes
	$query = "SELECT make,make_id FROM make ORDER BY make;";
	$result = mysql_query($query);
	while ($row = mysql_fetch_array($result)) {
		$makes[$row['make_id']] = $row['make'];
	}
	return $makes;
}

function getModels() {
	// Get an array of all the possible models
	$query = "SELECT model,model_id FROM model ORDER BY model;";
	$result = mysql_query($query);
	while ($row = mysql_fetch_array($result)) {
		$models[$row['model_id']] = $row['model'];
	}
	return $models;
}

//replace commas and dollarsigns in the textfield with empty strings, then check that the value is numeric before inserting into the DB.
function getCleanNumber($number) {
	$number = str_replace("$", "", $number);
	$number = str_replace(",","", $number);
	if (is_numeric($number))
		return $number;
	return false;
}

/** 
* moves the image to the ../external/images/ folder on the server, the path to which is pulled back from the DB
* @requires GD Library
* @param $file = The $_FILES array
* @return  false on error; new file name on success
*/
function moveUploadedImage($file, $newwidth, $newheight) {
	$name = '';
	
	if (isset($file) && $file['name'] != '') {
		$local_name =  "../external/images/" . basename($file['name']);
		$name = basename($file['name']);
		for ($i=0; file_exists($local_name); $i++) {
			$name = $i . basename($file['name']);
			$local_name = "../external/images/" . $name;
		}
		if (is_uploaded_file($file['tmp_name'])) {
			// Successfully uploaded and moved.
			// Get new sizes
			list($width, $height) = getimagesize($file['tmp_name']);

			// Process through GD library 
			if (!($thumb = imagecreatetruecolor($newwidth, $newheight))) return false;
			if (!($source = imagecreatefromjpeg($file['tmp_name']))) return false;

			// Resize
			if (!imagecopyresampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height)) return false;
			imagedestroy($source);
			// Output
			
			imagejpeg($thumb, $local_name);
			imagedestroy($thumb);
			return $name;
		}
		throw new Exception("File could not be resized!");
	}
	throw new Exception("unable to locate file!");
}

function uploadCarImg($inputIndex=0,$inputId='picture'){
    $imgToUpload = $_FILES[$inputId]['name'][$inputIndex];

    echo "<p>img: $imgToUpload</p>";

    if ($imgToUpload != ''){
    	$image_location = moveUploadedImage($imgToUpload, 640, 480);
    	if (!$image_location) {
    		echo "<b>New Image '" . $imgToUpload . "' was NOT uploaded successfully.</b> ". $_FILES[$inputId][$inputIndex]['error']."<br/>";
    	} 
    	else {
    		$query = "INSERT INTO car_img SET car_id='".$car_id."',image_location='".$image_location."';";
    		$result = mysql_query($query);
    		if ($result)
    			echo "<b>New Image '" . $imgToUpload . "' was uploaded and committed successfully.</b><br/>";
    		else
    			echo "<b>New Image '" . $imgToUpload . "' was uploaded successfully, but it was not committed.</b><br/>";
    	}
    } 
    else {
    	echo "<b>No new images added</b><br/>";
    }
}
?>