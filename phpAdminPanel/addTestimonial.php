<?php

//if submitted value is in the POST array start adding the new image & text
if (isset($_POST['submit'])) { 

	//if the input field from the form below is set in the $_FILES array, and not empty
	if (isset($_FILES['picture']) && trim($_FILES['picture']['name']) != '') {
		$imgName = moveUploadedImage($_FILES['picture'], 640, 480);

		//moveuploadedimage returns false if the upload fails
		if (!$imgName) {
			echo "<b>New Testimonial Image '" . $_FILES['picture']['name'] . "' was NOT uploaded successfully.</b> ". $_FILES['picture']['error']."<br/>";
		}
		
	}
	//form wasn't submitted 
	else {
		echo "<b>No new image added</b><br/>";
	}
	
	if (isset($imgName)) {
		$query = "INSERT INTO testimonials(image_location, data) VALUES('$imgName', '" . mysql_real_escape_string($_POST['caption']) . "');";
	
		if (!mysql_query($query))
			trigger_error("There was a SQL error: $query", E_USER_WARNING);
	} else
		echo "You must include an image with the testimonial";
}

?>
<form method="post" action="<?php echo "?page=$page" ?>" enctype="multipart/form-data">
	<input type="hidden" name="MAX_FILE_SIZE" value="7340000000" />

	<table>	
		<tr>
			<td>
				Picture: 
			</td>
			<td>
				<!-- Name of input element determines name in $_FILES array -->
				<input name="picture" type="file" />
			</td>
		</tr>
		<tr>
			<td>
				Caption:
			</td>
			<td>
				<textarea name="caption" cols="30" rows="5"></textarea>
			</td>
		</tr>
	</table>

	<input type="submit" name="submit" value="Add This Testimonial"/>
</form>