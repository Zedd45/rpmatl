<?php
header('Content-Type: image/jpeg'); #tell apache this is an image - must happen before anything else.  This will serve JPGS with png type, so I think the reverse is true, but I haven't tested it.
header("Cache-Control: max-age=86400"); #this isn't going to work until we tell apache that it's ok to cache PHP, or we mod rewrite this (which is a better option)

//now set it to expire 30 days in the future, so the browser will cache the image.
$days = 30;
$offset = 60 * 60 * 24 * $days;
$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";

header($ExpStr);


require_once '../phpObjects/SimpleImage.php';
require_once '../phpcfg/config.php';
require_once '../phpinc/globalUtils.php';

$fileName = strip_tags(($_GET['fileName']));

if (null === $fileName || '' === $fileName){
  error_log("empty image request sent to thumbnail processor.");
  exit();
}

$imgPath = getFullImagePathByFileName($fileName);

// echo "File name is $fileName. full path: $imgPath";  #Gotta kill the mime type to see this.

if (false !== $imgPath){   
  $SI_handler = new SimpleImage();
  $SI_handler->load('..' . $imgPath);  #BUG: the path will blow up using file exists, but imageSize() inside of the load() method needs a relative path for whatever reason, thefore, append '..'.f 
  $SI_handler->resizeToWidth(THUMB_IMG_WIDTH);
  $SI_handler->output();
} 
else {
  header('HTTP/1.1 500 Internal Server Error');
}
?>